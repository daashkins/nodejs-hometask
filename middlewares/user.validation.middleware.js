const { user } = require('../models/user');
const {validateParams} = require('./validation')

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let requestParams = [
        {   
            param_key: 'firstName',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return param.length > 0}]
        },
        {   
            param_key: 'lastName',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return param.length > 0}]
        },
        {   
            param_key: 'email',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return param.indexOf('@gmail.com') !== -1}]
        }, {
            param_key: 'phoneNumber',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return param.indexOf('+380') !== -1 && param.length === 13}]
        },
        {
            param_key: 'password',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return param.length >= 3}]
        }
    ]
        validateParams(requestParams, req, res, next);

}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    let requestParams = [
        {   
            param_key: 'firstName',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return param.length > 0}]
        },
        {   
            param_key: 'lastName',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return param.length > 0}]
        },
        {   
            param_key: 'email',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return param.indexOf('@gmail.com') !== -1}]
        }, {
            param_key: 'phoneNumber',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return param.indexOf('+380') !== -1 && param.length === 13}]
        },
        {
            param_key: 'password',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return param.length >= 3}]
        }
    ]
        validateParams(requestParams, req, res, next);
        //next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;