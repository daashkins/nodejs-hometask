const { codes } = require('./codes');

const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err) {
        res.status(codes.ERROR_NOT_FOUND);
        res.send({"error":true, "message":res.err.message});
    } else {
        if (res.data) {
            res.send(res.data);
        } else {
            res.status(codes.ERROR_BAD_REQUEST);
            res.send({"error":true, "message":"One or more parameters are empty or not valid"});
        }
    }
    next();
}

exports.responseMiddleware = responseMiddleware;
