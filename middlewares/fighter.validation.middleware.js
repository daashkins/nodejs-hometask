const { fighter } = require('../models/fighter');
const {validateParams} = require('./validation')

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (!req.body.defense) {
        req.body.defense = getRandomInt(1, 10);
    }

    if (!req.body.health) {
        req.body.health = 100;
    }

    let requestParams = [
        {   
            param_key: 'power',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return parseInt(param) < 100}]
        },
        // {   
        //     param_key: 'defense',
        //     required: fasle,
        //     type: 'number',
        //     validator_functions: [(param) => {return param <= 10 && param >= 1}]
        // },
        {
            param_key: 'name',
            required: true,
            type: 'string',
            validator_functions: [(param) => {return param.length > 1}]
        }
    ]
        validateParams(requestParams, req, res, next);
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    let requestParams = [
        {   
            param_key: 'power',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return parseInt(param) < 100}]
        },
        {   
            param_key: 'defense',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return parseInt(param) <= 10 && parseInt(param)>= 1}]
        },
        {   
            param_key: 'health',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return parseInt(param) <= 100}]
        },
        {
            param_key: 'name',
            required: false,
            type: 'string',
            validator_functions: [(param) => {return param.length > 1}]
        }
    ]
        validateParams(requestParams, req, res, next);
}

function getRandomInt(min, max) {
    return (Math.random() * (max - min) + min);
  }

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;