const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(create) {
        UserRepository.create(create);
    }

    getById(search) {
        return UserRepository.getOne(search);
    }

    getAll () {
        return UserRepository.getAll();
    }

    updateUser (id, dataToUpdate) {
        UserRepository.update(id, dataToUpdate)
    }
     
    deleteUser (id) {
        UserRepository.delete(id)
    
    }

}

module.exports = new UserService();
