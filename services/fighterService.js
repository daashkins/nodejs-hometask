const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(create) {
       FighterRepository.create(create);
    }

    getById(search) {
        return FighterRepository.getOne(search);
    }

    getAll() {
        return FighterRepository.getAll();
    }

    update (id, dataToUpdate) {
        FighterRepository.update(id, dataToUpdate)
    }
     
    delete(id) {
        FighterRepository.delete(id)
    
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();