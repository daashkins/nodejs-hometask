const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('/', createFighterValid, function(req, res, next){ 
    try {  
        const fighter = FighterService.search({name: req.body.name});
        if(fighter) {
            throw Error('Fighter exists!');
        }
        FighterService.create(req.body);
        res.data = FighterService.search(req.body);

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware);


router.get('/:id', function(req, res, next){
      try {  
        const fighter = FighterService.getById({id: req.params.id});
        if(!fighter) {
            throw Error('Fighter doesn`t exist!');
        }
        res.data = fighter;

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware); 

router.get('/', function(req, res, next){
      try {  
        const fighters = FighterService.getAll();
        if(!fighters) {
            throw Error('Fighters don`t exist!');
        }
        res.data = fighters;

    } catch (error) {
        res.err = error;
    }
    next();
}, responseMiddleware); 

router.put('/:id', updateFighterValid, function(req, res, next){ 
    try {  
        const user = FighterService.getById({id: req.params.id});
        if(!user) {
            throw Error('Fighter don`t exist!');
        }
        FighterService.update(req.params.id, req.body);
        res.data = FighterService.getById({id: req.params.id});

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next){ 
    try {  
        const fighter = FighterService.getById({id: req.params.id});
        if(!fighter) {
            throw Error('Fighter don`t exist!');
        }
        FighterService.delete(req.params.id);
        res.data = FighterService.getAll();

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware);
module.exports = router;