const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.post('/', createUserValid, function(req, res, next){ 
    try {  
        const user = UserService.search({email: req.body.email});
        if(user) {
            throw Error('User exists!');
        }
        UserService.create(req.body);
        res.data = UserService.search(req.body);

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware);


router.get('/:id', function(req, res, next){
      try {  
        const user = UserService.getById({id: req.params.id});
        if(!user) {
            throw Error('User doesn`t exist!');
        }
        res.data = user;

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware); 

router.get('/', function(req, res, next){
      try {  
        const users = UserService.getAll();
        if(!users) {
            throw Error('Users don`t exist!');
        }
        res.data = users;

    } catch (error) {
        res.err = error;
    }
    next();
}, responseMiddleware); 

router.put('/:id', updateUserValid, function(req, res, next){ 
    try {  
        const user = UserService.getById({id: req.params.id});
        if(!user) {
            throw Error('User don`t exist!');
        }
        UserService.updateUser(req.params.id, req.body);
        res.data = UserService.getById({id: req.params.id});

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next){ 
    try {  
        const user = UserService.getById({id: req.params.id});
        if(!user) {
            throw Error('User don`t exist!');
        }
        UserService.deleteUser(req.params.id);
        res.data = UserService.getAll();

    } catch (error) {
        res.err = error;
    }
    
    next();
}, responseMiddleware);

module.exports = router;
